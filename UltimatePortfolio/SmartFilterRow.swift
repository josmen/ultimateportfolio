//
//  SmartFilterRow.swift
//  UltimatePortfolio
//
//  Created by Jose Antonio Mendoza on 24/5/23.
//

import SwiftUI

struct SmartFilterRow: View {
    var filter: Filter

    var body: some View {
        NavigationLink(value: filter) {
            Label(filter.name, systemImage: filter.icon)
        }
    }
}
